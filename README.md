# Test Pyqtgraph

Repository to store tests of `pyqtgraph` library https://www.pyqtgraph.org/

## Install environnement

```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

## Run test program
```bash
source venv/bin/activate
python main.py
```

## Added classes for `pyqtgraph`

# `SvgBarGraphItem`

New class inheriting `BarGraphItem` for svg brush use.

- create QImage from wanted .svg file / color / size and use it for brush texture image
- update QBrush transform for scale depending on current zoom with `viewTransformChanged` override
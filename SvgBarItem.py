import pyqtgraph as pq
from PyQt5.QtCore import QRectF
from PyQt5.QtGui import QBrush, QImage, QColor, QPainter, QTransform
from PyQt5.QtSvg import QSvgRenderer


class SvgBarGraphItem(pq.BarGraphItem):

    def __init__(self, **opts):
        pq.BarGraphItem.__init__(self, **opts)

        self.images = {}
        self.svg_sizes = None
        if "svg_files" in opts:
            self.svg_files = opts["svg_files"]
        if "svg_sizes" in opts:
            self.svg_sizes = opts["svg_sizes"]

        if self.svg_files is not None:
            self._define_brush_texture()

    def _define_brush_texture(self):

        default_brush = self.opts['brushes']
        self.opts['brushes'] = []

        for i in range(len(self.svg_files)):
            color = default_brush[i]
            svg_file = self.svg_files[i]
            svg_size = 150
            if self.svg_sizes is not None:
                svg_size = self.svg_sizes[i]
            if (svg_file, svg_size, color) not in self.images:
                self._render_svg_image(svg_file, svg_size, color)

            brush = QBrush()
            brush.setTextureImage(self.images[(svg_file, svg_size, color)])
            self.opts['brushes'].append(brush)

    def _render_svg_image(self, svg_file, svg_size, color: str):
        renderer = QSvgRenderer(svg_file)
        default_size = renderer.defaultSize()
        ratio = default_size.height() / default_size.width()
        hSize = svg_size * ratio
        image = QImage(svg_size, hSize, QImage.Format_ARGB32_Premultiplied)
        image.fill(QColor(color))
        impainter = QPainter(image)
        renderer.render(impainter)
        impainter.end()
        self.images[(svg_file, svg_size, color)] = image

    def viewTransformChanged(self):
        for i in range(len(self.opts['brushes'])):
            wanted_pix_w = 150
            wanted_pix_h = wanted_pix_w

            svg_rect = QRectF(0, 0, wanted_pix_w, wanted_pix_h)
            svg_rect_device = self.mapRectFromDevice(svg_rect)

            ratio_w = wanted_pix_w / svg_rect_device.width()
            ratio_h = wanted_pix_h / svg_rect_device.height()

            brush = self.opts['brushes'][i]
            transform = QTransform()
            transform = transform.scale(1 / ratio_w, 1 / ratio_h)
            brush.setTransform(transform)
            self.opts['brushes'][i] = brush

        self.picture = None

    def _get_item_attribute(self, i: int):
        svg_file = None
        if self.svg_files is not None:
            svg_file = self.svg_files[i]
        return svg_file

    def _get_item_svg_file(self, i: int) -> str:
        svg_file = None
        if self.svg_files is not None:
            svg_file = self.svg_files[i]
        return svg_file

    def _get_item_svg_size(self, i: int) -> str:
        svg_file = None
        if self.svg_files is not None:
            svg_file = self.svg_files[i]
        return svg_file

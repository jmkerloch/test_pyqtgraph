import sys
import numpy as np
import pyqtgraph as pg

from PyQt5 import QtWidgets, QtCore

import pyqtgraph.parametertree as ptree
import pyqtgraph.parametertree.parameterTypes as pTypes
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QDialog

from SvgBarItem import SvgBarGraphItem


class ParametersWidget(QDialog):

    def __init__(self, parent, plot_item):
        super(QWidget, self).__init__(parent)

        layout = QHBoxLayout()
        params = ptree.Parameter.create(name='Parameters', type='group')

        pen_param = pTypes.PenParameter(name='Pen')
        pen_param.sigValueChanged.connect(lambda params, changes: plot_item.setPen(changes))
        symb_param = pTypes.ListParameter(name='Symbol', limits=['o', 't', 't1', 't2', 't3', 's'])
        symb_param.sigValueChanged.connect(lambda params, changes: plot_item.setSymbol(changes))

        params.addChild(pen_param)
        params.addChild(symb_param)

        t = pg.parametertree.ParameterTree()
        t.setParameters(params, showTop=False)
        layout.addWidget(t)
        self.setLayout(layout)


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        # self.cw = QWidget()
        self.main_layout = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        # self.cw.setLayout(self.main_layout)
        self.setCentralWidget(self.main_layout)

        self._conf_widget = []

        self.strat_plot = None
        self.depth_layout = QtWidgets.QSplitter()
        self.main_layout.addWidget(self.depth_layout)
        self._add_depth_assays()

        self.ref_time_plot = None
        self.time_layout = QtWidgets.QSplitter(QtCore.Qt.Vertical)
        self.main_layout.addWidget(self.time_layout)
        self._add_time_assays()

    def _add_depth_assays(self):
        self.strat_plot = pg.PlotWidget(name="Stratography", title="Stratography")
        self.strat_plot.setMouseEnabled(x=False)  # Only allow zoom in Y-axis
        self.strat_plot.setBackground('white')
        self.strat_plot.hideAxis('right')
        self.strat_plot.hideAxis('bottom')
        self.depth_layout.addWidget(self.strat_plot)
        self.add_bar()
        size = 100000
        total_height = 10 * 3
        y = np.linspace(0, total_height, size)
        self.add_depth_plot(self.create_assay_plot(title="Assay 1", x=np.random.normal(size=size), y=y))
        self.add_depth_plot(self.create_assay_plot(title="Assay 2", x=np.random.normal(size=size), y=y))

    def _add_time_assays(self):
        size = 10000
        self.ref_time_plot = self.create_assay_plot(title="Assay 1", y=np.random.normal(size=size))
        self.add_time_plot(self.ref_time_plot)
        self.add_time_plot(self.create_assay_plot(title="Assay 2", y=np.random.normal(size=size)))

        x = np.linspace(0, size, size - 1)
        y = np.random.normal(size=size - 1)
        self.add_time_plot(self.create_histo_assay_plot(title="Histo", x=x, y=y))

        x = np.array(list(zip(x, x + 1)))
        self.add_time_plot(self.create_bar_assay_plot(title="Bars", x=x, y=y))

        x = np.linspace(0, size, size)
        multiple_assay_plot = self.create_assay_plot(title="Assay 2", x=x, y=np.sin(x))
        self.add_time_plot(multiple_assay_plot)
        item = multiple_assay_plot.plot(y=np.cos(x), x=x, pen='green')
        self._add_parameters_widget(multiple_assay_plot, item, 'sinusoide configuration')

    def create_assay_plot(self, title: str, x=None, y=None) -> pg.PlotWidget:
        plot_widget = pg.PlotWidget(name=title, title=title)
        plot_widget.setBackground('white')
        item = plot_widget.plot(x=x, y=y)
        self._add_parameters_widget(plot_widget, item)

        return plot_widget

    def create_histo_assay_plot(self, title: str, x=None, y=None) -> pg.PlotWidget:
        plot_widget = pg.PlotWidget(name=title, title=title)
        plot_widget.setBackground('white')
        item = plot_widget.plot(x=x, y=y, stepMode='left', fillLevel=0, fillOutline=True, brush=(0, 0, 255, 150))
        self._add_parameters_widget(plot_widget, item)
        return plot_widget

    def _add_parameters_widget(self, plot_widget, plot_item, action_str: str = 'Display configuration'):
        display_conf_widget = plot_widget.getPlotItem().getViewBox().menu.addAction(action_str)
        widget = ParametersWidget(self, plot_item)
        self._conf_widget.append(widget)
        display_conf_widget.triggered.connect(widget.show)

    def create_bar_assay_plot(self, title: str, x=None, y=None) -> pg.PlotWidget:
        plot_widget = pg.PlotWidget(name=title, title=title)
        plot_widget.setBackground('white')
        item = pg.BarGraphItem(x0=x[:, 0], x1=x[:, 1], height=y)
        # plot data: x, y values
        plot_widget.addItem(item)
        self._add_parameters_widget(plot_widget, item)
        return plot_widget

    def add_depth_plot(self, plot: pg.PlotWidget) -> None:
        self.depth_layout.addWidget(plot)
        plot.getViewBox().invertY(True)
        plot.setYLink(self.strat_plot)

    def add_time_plot(self, plot: pg.PlotWidget) -> None:
        self.time_layout.addWidget(plot)
        plot.setMouseEnabled(y=False)  # Only allow zoom in X-axis
        if self.ref_time_plot:
            plot.setXLink(self.ref_time_plot)

    def add_bar(self):
        svg_with = 10
        width = svg_with
        height = width

        x0_val = np.array([0, 0, 0])
        y0_val = np.array([0, height, height * 2.0])

        height_val = np.array([height, height, height / 2.0])
        width_val = np.array([width, width, width])
        brushes_val = np.array(['red', "blue", 'yellow'])
        svg_files = np.array(['styles/usgs733.svg', 'styles/usgs733.svg', 'styles/usgs733.svg'])
        svg_sizes = np.array([300, 200, 100])
        text = np.array(["usgs733/300", "usgs635/200", "usgs638/100"])

        bars = SvgBarGraphItem(height=height_val, width=width_val, x0=x0_val, y0=y0_val,
                               brushes=brushes_val, svg_files=svg_files, svg_sizes=svg_sizes)
        # plot data: x, y values
        self.strat_plot.addItem(bars)
        bars.getViewBox().invertY(True)

        text_bar_width = width_val * 0.5
        text_bar_x0 = x0_val + width

        bars = pg.BarGraphItem(height=height_val, width=text_bar_width, x0=text_bar_x0, y0=y0_val,
                               brushes=brushes_val)
        # plot data: x, y values
        self.strat_plot.addItem(bars)

        for i in range(len(text)):
            t = text[i]
            x = text_bar_x0[i] + text_bar_width[i] / 2.0
            y = y0_val[i] + height_val[i] / 2.0

            text_item = pg.TextItem(t, rotateAxis=(1, 0), angle=0)
            self.strat_plot.addItem(text_item)
            text_item.setPos(x, y)

        self.strat_plot.setLimits(xMin=0, xMax=svg_with + svg_with * 0.5)
        bars.getViewBox().invertY(True)


def main():
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
